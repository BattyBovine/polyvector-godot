#ifndef POLYVECTOR_SERVER_H
#define POLYVECTOR_SERVER_H

#include <core/object.h>
#include <scene/3d/mesh_instance.h>

typedef Map<int8_t, Ref<ArrayMesh> > MeshQuality;
typedef Map<uint16_t, MeshQuality> MeshEntry;
typedef Map<String, MeshEntry> MeshLibrary;

typedef Map<int8_t, AABB> AABBQuality;
typedef Map<uint16_t, AABBQuality> AABBFrame;
typedef Map<String, AABBFrame> AABBLibrary;

typedef Map<uint16_t, MeshInstance*> MeshInstanceMap;

class PolyVectorServer : public Object {
	GDCLASS(PolyVectorServer, Object);

	static PolyVectorServer *singleton;
	Vector2 vBaseResolution;

	MeshLibrary mlMeshes;
	AABBLibrary aabblBounds;

public:
	PolyVectorServer() { singleton=this; }
	~PolyVectorServer() { singleton=NULL; }
	static PolyVectorServer *get_singleton() { return singleton; }

	void set_mesh(const Ref<ArrayMesh> mesh, const String &hash, const uint16_t id, const int8_t lod) { this->mlMeshes[hash][id][lod] = mesh; }
	const Ref<ArrayMesh> get_mesh(const String &hash, const uint16_t id, const int8_t lod) {
		if(this->has_mesh(hash,id,lod))
			return this->mlMeshes[hash][id][lod];
		return Ref<ArrayMesh>();
	}
	bool has_mesh(const String &hash, const uint16_t id, const int8_t lod) { return (this->mlMeshes.has(hash) && this->mlMeshes[hash].has(id) && this->mlMeshes[hash][id].has(lod)); }
	void clear_mesh_cache() {
		for(MeshLibrary::Element *l=this->mlMeshes.front(); l; l=l->next()) {
			for(MeshEntry::Element *d=l->get().front(); d; d=d->next()) {
				for(MeshQuality::Element *m=d->get().front(); m; m=m->next())
					m->get().unref();
				d->get().clear();
			}
		}
	}

	void set_aabb(const AABB &bounds, const String &hash, const uint16_t frame, const int8_t lod) { this->aabblBounds[hash][frame][lod] = bounds; }
	const AABB get_aabb(const String &hash, const uint16_t frame, const int8_t lod) {
		if(this->has_aabb(hash,frame,lod))
			return this->aabblBounds[hash][frame][lod];
		return AABB();
	}
	bool has_aabb(const String &hash, const uint16_t frame, const int8_t lod) { return (this->aabblBounds.has(hash) && this->aabblBounds[hash].has(frame) && this->aabblBounds[hash][frame].has(lod)); }

	real_t get_lod_scale(Vector2 window) {
		if(this->vBaseResolution==Vector2())
			this->vBaseResolution = Vector2(ProjectSettings::get_singleton()->get("display/window/size/width"), ProjectSettings::get_singleton()->get("display/window/size/height"));
		return ((this->vBaseResolution.x / window.x) + (this->vBaseResolution.y / window.y)) / 2.0;
	}
};

#endif	// POLYVECTOR_SERVER_H
