# PolyVector
PolyVector is a proof-of-concept for importing SWF animations and triangulating them into polygons rendered with the GPU. It is implemented as a node module for Godot Game Engine 3.0.

Shape and path data is stored in memory using a Curve2D object, which are tessellated and triangulated as necessary when the quality level is changed. Automatic LOD and intelligent point reduction are both available. Animation works, but requires manual setup at the moment.

[A short feature demonstration is available on YouTube.](https://www.youtube.com/watch?v=azgkyvVQJcs)

To be completed:
================
1. Conversion from SWF line segments into complete shapes is still imperfect.
2. Gradients and colour transformations are not supported yet.
3. Masks are not supported, and I have no idea how to make it work. It may have to wait until Godot shaders gain support for custom render targets, or if stencil buffer support can be added somehow.

Additional notes:
=================
The SVG icon file at the root of the module directory needs to be copied to `editor/icons` in the engine's source directory before recompiling the engine if you want the custom icon to appear. It may also be necessary to delete `editor/editor_icons.gen.h` so that `scons` can regenerate the file.
