#include "polyvector.h"

PolyVector::PolyVector()
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder = Mutex::create();
	#endif

	this->materialDefault.instance();
	this->materialDefault->set_flag(SpatialMaterial::FLAG_ALBEDO_FROM_VERTEX_COLOR, true);
	this->materialDefault->set_cull_mode(SpatialMaterial::CULL_DISABLED);
	this->materialDefault->set_diffuse_mode(SpatialMaterial::DIFFUSE_OREN_NAYAR);
	this->materialDefault->set_roughness(1.0);
	this->materialDefault->set_metallic(0.0);
	this->materialDefault->set_specular(0.0);

	this->set_process_internal(true);
}

PolyVector::~PolyVector()
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	if(this->threadMeshBuilder) {
		Thread::wait_to_finish(this->threadMeshBuilder);
		memdelete(this->threadMeshBuilder);
	}
	#endif
	if(!this->materialDefault.is_null())
		this->materialDefault.unref();

	#ifndef NO_THREADS
	this->mutexMeshBuilder->unlock();
	memdelete(this->mutexMeshBuilder);
	#endif
}



void PolyVector::_notification(int p_what)
{
	switch (p_what) {
	case NOTIFICATION_ENTER_TREE:
		#ifdef TOOLS_ENABLED
		if(Engine::get_singleton()->is_editor_hint())
			this->iCurrentLod = this->iMaxLod;
		else
		#endif
		this->_get_lod_from_camera_distance();
		this->_draw_current_frame(true);
		break;
	case NOTIFICATION_INTERNAL_PROCESS:
		this->_get_lod_from_camera_distance();
		this->_draw_current_frame();
		break;
	}
}

void PolyVector::_get_lod_from_camera_distance()
{
	const Camera *camera = this->get_viewport()->get_camera();
	if(!camera) return;
	Vector3 camerapos = camera->get_global_transform().get_origin();
	const Vector3 pvpos = this->get_global_transform().get_origin();
	camerapos.y = pvpos.y;
	const real_t distancescale = POLYVECTOR_MAX_QUALITY - (Math::pow((camerapos.distance_to(pvpos) * PolyVectorServer::get_singleton()->get_lod_scale(this->get_viewport()->get_size())) / this->rLodDistanceScale, this->rLodDistanceCurve));
	const int8_t newlod = Math::ceil(CLAMP(Math::range_lerp(distancescale, -POLYVECTOR_MAX_QUALITY, POLYVECTOR_MAX_QUALITY, -POLYVECTOR_MAX_QUALITY, this->iMaxLod), -POLYVECTOR_MAX_QUALITY, POLYVECTOR_MAX_QUALITY));
	if(this->iCurrentLod!=newlod)
		this->bFrameDirty = true;
	this->iCurrentLod = newlod;
}

void PolyVector::_draw_current_frame(bool forcedirty)
{
	if(this->dataVectorFile.is_null() || (!forcedirty && !this->bFrameDirty))
		return;
	this->bFrameDirty = false;

	//#ifdef NO_THREADS
	this->_thread_mesh_builder_start(this);
	//#else
	//if(this->threadMeshBuilder) {
	//	Thread::wait_to_finish(this->threadMeshBuilder);
	//	memdelete(this->threadMeshBuilder);
	//}
	//this->threadMeshBuilder = Thread::create(PolyVector::_thread_mesh_builder_start, this);
	//#endif
}

void PolyVector::_clear_mesh_instances()
{
	int32_t childcount = this->get_child_count();
	for(int32_t i=childcount-1; i>=0; i--) {
		MeshInstance *mi = Node::cast_to<MeshInstance>(this->get_child(i));
		if(mi) this->remove_child(mi);
	}
	for(MeshInstanceMap::Element *mim=this->mapMeshDisplay.front(); mim; mim=mim->next())
		memdelete<MeshInstance>(mim->get());
	this->mapMeshDisplay.clear();
}


void PolyVector::_thread_mesh_builder_start(void *pv_in)
{
	PolyVector *pv = (PolyVector*)pv_in;
	const uint16_t frame = pv->iCurrentFrame;
	const int8_t lod = pv->iCurrentLod;
	if(pv->_thread_mesh_builder())
		pv->_thread_mesh_builder_finish(frame, lod);
}
void PolyVector::_thread_mesh_builder_finish(uint16_t frame, int8_t lod)
{
	for(MeshInstanceMap::Element *m=this->mapMeshDisplay.front(); m; m=m->next())
		m->get()->set_visible(false);
	float layer_separation = 0.0;
	AABB bounds;
	for(PolyVectorFrame::Element *c=this->lFrameData[frame].front(); c; c=c->next(),layer_separation+=this->rLayerDepth) {
		const PolyVectorSymbol symbol = c->get();
		Transform transform;
		transform.set(
			symbol.matrix.ScaleX,		symbol.matrix.Skew1,		0.0,
			symbol.matrix.Skew0,		symbol.matrix.ScaleY,		0.0,
			0.0,						0.0,						1.0,
			symbol.matrix.TranslateX,	-symbol.matrix.TranslateY,	layer_separation
		);

		MeshInstance *mi = NULL;
		if(this->mapMeshDisplay.has(symbol.depth)) {
			mi = this->mapMeshDisplay[symbol.depth];
		} else {
			mi = memnew(MeshInstance);
			this->add_child(mi);
			mi->set_owner(this);
			this->mapMeshDisplay[symbol.depth] = mi;
		}
		mi->set_transform(transform);
		mi->set_visible(true);
		mi->set_material_override(this->materialDefault);
		mi->set_cast_shadows_setting(this->scsCastShadows);
		Ref<ArrayMesh> m = PolyVectorServer::get_singleton()->get_mesh(this->dataVectorFile->get_hash(), symbol.id, lod);
		mi->set_mesh(m);

		if(!PolyVectorServer::get_singleton()->has_aabb(this->dataVectorFile->get_hash(), this->iCurrentFrame, this->iCurrentLod)) {
			if(c==this->lFrameData[frame].front())
				bounds = transform.xform(m->get_custom_aabb());
			else
				bounds.merge_with(transform.xform(m->get_custom_aabb()));
		}
	}
	if(bounds.get_size()!=Vector3())
		PolyVectorServer::get_singleton()->set_aabb(bounds, this->dataVectorFile->get_hash(), this->iCurrentFrame, this->iCurrentLod);
	#ifndef NO_THREADS
	if(this->threadMeshBuilder) {
		Thread::wait_to_finish(this->threadMeshBuilder);
		memdelete(this->threadMeshBuilder);
		this->threadMeshBuilder = NULL;
	}
	#endif
}
bool PolyVector::_thread_mesh_builder()
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	#endif

	if(this->dataVectorFile.is_null() || this->iCurrentFrame>this->lFrameData.size())
		return false;
	for(PolyVectorFrame::Element *c=this->lFrameData[this->iCurrentFrame].front(); c; c=c->next()) {
		PolyVectorSymbol symbol = c->get();
		if(!PolyVectorServer::get_singleton()->has_mesh(this->dataVectorFile->get_hash(), symbol.id, this->iCurrentLod)) {
			PolyVectorCharacter *pvchar = &this->lDictionaryData[symbol.id];
			Array arr;
			arr.resize(Mesh::ARRAY_MAX);
			Ref<ArrayMesh> newmesh;	// Create one new mesh to represent each symbol
			newmesh.instance();
			for(PolyVectorShapeList::Element *s=pvchar->shapes.front(); s; s=s->next()) {
				PolyVectorShape &shape = s->get();
				if(shape.fillcolour==NULL)
					continue;
				std::vector< std::vector<Vector2> > polygons;
				PoolVector2Array tess;
				if(shape.path.curve.get_point_count() <= 3)
					for(int i=0; i<shape.path.curve.get_point_count(); i++)
						tess.append(shape.path.curve.get_point_position(i));
				else if(this->iCurrentLod >= 0)
					tess = shape.path.curve.tessellate(this->iCurrentLod, this->rMaxTessellationAngle);
				else
					//tess = this->vw_simplify(shape.path.curve.tessellate(0, 0), abs(this->iCurrentLod));
					tess = this->rdp_simplify(shape.path.curve.tessellate(0, 0), abs(this->iCurrentLod) * this->rLodEpsilon);
				//if(!shape.path.closed) {	// If shape is not a closed loop, store as a stroke
				//	shape.strokes[this->iCurveQuality].push_back(tess);
				//} else {					// Otherwise, triangulate
				uint32_t tess_size = tess.size();
				std::vector<Vector2> poly;
				PoolVector2Array::Read tessreader = tess.read();
				for(uint32_t i=1; i<tess_size; i++)	// Push vertices into a standard vector
					poly.push_back(tessreader[i]);
				polygons.push_back(poly);
				std::vector<Vector2> tessverts;
				tessverts.insert(tessverts.end(), poly.begin(), poly.end());
				for(List<uint16_t>::Element *hole=shape.holes.front(); hole; hole=hole->next()) {
					PoolVector2Array holetess = pvchar->shapes[hole->get()].path.curve.tessellate(this->iCurrentLod, this->rMaxTessellationAngle);
					uint32_t holetess_size = holetess.size();
					std::vector<Vector2> holepoly;
					PoolVector2Array::Read holetessreader = holetess.read();
					for(uint32_t j=0; j<holetess_size; j++)
						holepoly.push_back(holetessreader[j]);
					polygons.push_back(holepoly);
					tessverts.insert(tessverts.end(), holepoly.begin(), holepoly.end());
				}
				//}
				PoolVector2Array vertices;	// Create a new pool of vertices for each individual shape
				PoolVector3Array normals;
				PoolVector<Color> colours;
				PoolIntArray indices;
				if(!polygons.empty()) {
					std::vector<N> earindices = mapbox::earcut<N>(polygons);
					if(earindices.empty())
						continue;
					uint32_t idxcount = 0;
					const Vector3 nrm(0.0, 0.0, 1.0);
					const Color clr = shape.fillcolour->to_linear();
					for(std::vector<N>::reverse_iterator idx=earindices.rbegin(); idx!=earindices.rend(); idx++) {	// Add vertices in reverse so the normals are facing forward
						indices.push_back(idxcount++);
						colours.push_back(clr);
						normals.push_back(nrm);
						vertices.push_back(tessverts[*idx]);
					}
				}
				arr[Mesh::ARRAY_VERTEX] = vertices;
				arr[Mesh::ARRAY_NORMAL] = normals;
				arr[Mesh::ARRAY_COLOR] = colours;
				arr[Mesh::ARRAY_INDEX] = indices;

				newmesh->add_surface_from_arrays(Mesh::PRIMITIVE_TRIANGLES, arr, Array(), Mesh::ARRAY_FLAG_USE_2D_VERTICES);
				//newmesh->surface_set_material(surfacecount++, this->materialDefault);
			}
			AABB bounds;
			const Vector2 pos(pvchar->bounds.get_position());
			const Vector2 size(pvchar->bounds.get_size());
			bounds.set_position(Vector3(pos.x, pos.y, 0.0));
			bounds.set_size(Vector3(size.x, size.y, 0.0));
			newmesh->set_custom_aabb(bounds);
			PolyVectorServer::get_singleton()->set_mesh(newmesh, this->dataVectorFile->get_hash(), symbol.id, this->iCurrentLod);
		}
	}

	#ifndef NO_THREADS
	this->mutexMeshBuilder->unlock();
	#endif
	return true;
}


void PolyVector::set_vector_image(const Ref<JSONVector> &p_vector)
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	#endif

	this->dataVectorFile = p_vector;
	this->_clear_mesh_instances();
	if(this->dataVectorFile.is_null())
		return;
	this->rFps = this->dataVectorFile->get_fps();
	this->lFrameData = this->dataVectorFile->get_frames();
	this->lDictionaryData = this->dataVectorFile->get_dictionary();

	AnimationPlayer *animplayer = NULL;
	int32_t childcount = this->get_child_count();
	for(int32_t i=0; i<childcount; i++) {
		animplayer = Node::cast_to<AnimationPlayer>(this->get_child(i));
		if(animplayer)	break;
	}

	this->set_time(0.0);

	#ifndef NO_THREADS
	this->mutexMeshBuilder->unlock();
	#endif
}

void PolyVector::set_time(real_t t)
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	#endif
	this->rTime=t;
	uint16_t frameno = CLAMP(this->rFps*this->rTime, 0, this->lFrameData.size()-1);
	if(frameno != this->iCurrentFrame) {
		this->iCurrentFrame = frameno;
		this->bFrameDirty = true;
	}
	#ifndef NO_THREADS
	this->mutexMeshBuilder->unlock();
	#endif
}

void PolyVector::set_lod_max_quality(int8_t t)
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	#endif
	this->iMaxLod = t;
	#ifdef TOOLS_ENABLED
	if(Engine::get_singleton()->is_editor_hint()) {
		this->iCurrentLod = this->iMaxLod;
		this->bFrameDirty = true;
	}
	#endif
	#ifndef NO_THREADS
	this->mutexMeshBuilder->unlock();
	#endif
}

void PolyVector::set_lod_distance_scale(real_t s)
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	#endif
	this->rLodDistanceScale=s;
	#ifndef NO_THREADS
	this->mutexMeshBuilder->unlock();
	#endif
}

void PolyVector::set_lod_distance_curve(real_t c)
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	#endif
	this->rLodDistanceCurve=c;
	#ifndef NO_THREADS
	this->mutexMeshBuilder->unlock();
	#endif
}

void PolyVector::set_lod_reduction_factor(real_t e)
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	#endif
	this->rLodEpsilon=e;
	#ifndef NO_THREADS
	this->mutexMeshBuilder->unlock();
	#endif
}

void PolyVector::set_layer_separation(real_t d)
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	#endif
	this->rLayerDepth=d;
	#ifndef NO_THREADS
	this->mutexMeshBuilder->unlock();
	#endif
	this->bFrameDirty = true;
}

void PolyVector::set_material(const Ref<Material> &p_material)
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	#endif
	this->materialDefault=p_material;
	#ifndef NO_THREADS
	this->mutexMeshBuilder->unlock();
	#endif
}

void PolyVector::set_cast_shadow(GeometryInstance::ShadowCastingSetting cs) {
	this->scsCastShadows = cs;
}

void PolyVector::set_max_tessellation_angle(real_t f)
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	#endif
	this->rMaxTessellationAngle=f;
	#ifndef NO_THREADS
	this->mutexMeshBuilder->unlock();
	#endif
}


PoolVector<Face3> PolyVector::get_faces(uint32_t p_usage_flags) const
{
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	#endif
	if(this->pvFaces.size() <= 0)
		for(MeshInstanceMap::Element *mi=this->mapMeshDisplay.front(); mi; mi=mi->next())
			this->pvFaces.append_array(mi->get()->get_mesh()->get_faces());
	#ifndef NO_THREADS
	this->mutexMeshBuilder->lock();
	#endif
	return this->pvFaces;
}



void PolyVector::_bind_methods()
{
	ClassDB::bind_method(D_METHOD("set_vector_image"), &PolyVector::set_vector_image);
	ClassDB::bind_method(D_METHOD("get_vector_image"), &PolyVector::get_vector_image);
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "vector", PROPERTY_HINT_RESOURCE_TYPE, "JSONVector"), "set_vector_image", "get_vector_image");

	ClassDB::bind_method(D_METHOD("set_time"), &PolyVector::set_time);
	ClassDB::bind_method(D_METHOD("get_time"), &PolyVector::get_time);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "timecode", PROPERTY_HINT_RANGE, "0.0, 1000.0, 0.01, 0.0"), "set_time", "get_time");

	
	ADD_GROUP("Level Of Detail","lod_");
	ClassDB::bind_method(D_METHOD("set_lod_max_quality"), &PolyVector::set_lod_max_quality);
	ClassDB::bind_method(D_METHOD("get_lod_max_quality"), &PolyVector::get_lod_max_quality);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "lod_max_quality", PROPERTY_HINT_RANGE, "0,10,1,2"), "set_lod_max_quality", "get_lod_max_quality");

	ClassDB::bind_method(D_METHOD("set_lod_distance_scale"), &PolyVector::set_lod_distance_scale);
	ClassDB::bind_method(D_METHOD("get_lod_distance_scale"), &PolyVector::get_lod_distance_scale);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "lod_distance_scale", PROPERTY_HINT_RANGE, "0.01, 100.0, 0.01, 1.0"), "set_lod_distance_scale", "get_lod_distance_scale");

	ClassDB::bind_method(D_METHOD("set_lod_distance_curve"), &PolyVector::set_lod_distance_curve);
	ClassDB::bind_method(D_METHOD("get_lod_distance_curve"), &PolyVector::get_lod_distance_curve);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "lod_distance_curve", PROPERTY_HINT_EXP_EASING), "set_lod_distance_curve", "get_lod_distance_curve");

	ClassDB::bind_method(D_METHOD("set_lod_reduction_factor"), &PolyVector::set_lod_reduction_factor);
	ClassDB::bind_method(D_METHOD("get_lod_reduction_factor"), &PolyVector::get_lod_reduction_factor);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "lod_reduction_factor", PROPERTY_HINT_RANGE, "0.001, 1.0, 0.001, 0.025"), "set_lod_reduction_factor", "get_lod_reduction_factor");


	ADD_GROUP("Adjustments", "");
	ClassDB::bind_method(D_METHOD("set_material"), &PolyVector::set_material);
	ClassDB::bind_method(D_METHOD("get_material"), &PolyVector::get_material);
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "material", PROPERTY_HINT_RESOURCE_TYPE, "SpatialMaterial"), "set_material", "get_material");

	ClassDB::bind_method(D_METHOD("set_cast_shadow", "cast_shadow"), &PolyVector::set_cast_shadow);
	ClassDB::bind_method(D_METHOD("get_cast_shadow"), &PolyVector::get_cast_shadow);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cast_shadow", PROPERTY_HINT_ENUM, "Off,On,Double-Sided,Shadows Only"), "set_cast_shadow", "get_cast_shadow");

	ClassDB::bind_method(D_METHOD("set_layer_separation"), &PolyVector::set_layer_separation);
	ClassDB::bind_method(D_METHOD("get_layer_separation"), &PolyVector::get_layer_separation);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "layer_separation", PROPERTY_HINT_RANGE, "0.0, 1.0, 0.0001, 0.001"), "set_layer_separation", "get_layer_separation");

	ClassDB::bind_method(D_METHOD("set_max_tessellation_angle"), &PolyVector::set_max_tessellation_angle);
	ClassDB::bind_method(D_METHOD("get_max_tessellation_angle"), &PolyVector::get_max_tessellation_angle);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "max_tessellation_angle", PROPERTY_HINT_RANGE, "1.0, 10.0, 0.1, 4.0"), "set_max_tessellation_angle", "get_max_tessellation_angle");


	#ifdef POLYVECTOR_DEBUG
	ADD_GROUP("Debug", "");
	ClassDB::bind_method(D_METHOD("set_debug_wireframe"), &PolyVector::set_debug_wireframe);
	ClassDB::bind_method(D_METHOD("get_debug_wireframe"), &PolyVector::get_debug_wireframe);
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "debug_wireframe"), "set_debug_wireframe", "get_debug_wireframe");
	#endif

	ClassDB::bind_method(D_METHOD("get_current_lod"), &PolyVector::get_current_lod);
	ClassDB::bind_method(D_METHOD("get_vertex_count"), &PolyVector::get_vertex_count);
}



PoolVector2Array PolyVector::vw_simplify(const PoolVector2Array &pointlist, uint8_t factor)
{
	PoolVector2Array pointsout;
	pointsout.append_array(pointlist);
	const PoolVector2Array::Read points = pointsout.read();
	const size_t pointcount = pointlist.size();

	uint32_t killcount = pointcount - CLAMP(pointcount/(factor+1), 3, pointcount);
	while(killcount-- > 0) {
		uint32_t index = 1;
		real_t minimumarea = this->area_of_triangle(points[0], points[1], points[2]);
		for(uint32_t i=2; i<(pointsout.size()-2); i++) {
			real_t area = this->area_of_triangle(points[i-1], points[i], points[i+1]);
			if(area < minimumarea) {
				minimumarea = area;
				index = i;
			}
		}
		pointsout.remove(index);
	}

	return pointsout;
}
real_t PolyVector::area_of_triangle(Vector2 a, Vector2 b, Vector2 c)
{
	return abs((a.x * (b.y-c.y)) + (b.x * (c.y-a.y)) + (c.x * (a.y-b.y))) / 2.0;
}

PoolVector2Array PolyVector::rdp_simplify(const PoolVector2Array &pointlist, double epsilon)
{
	if(pointlist.size() < 3)
		return pointlist;

	PoolVector2Array out;

	// Find the point with the maximum distance from line between start and end
	double dmax = 0.0;
	size_t index = 0;
	const size_t end = pointlist.size()-1;
	for(size_t i=1; i<end; i++) {
		double d = this->perpendicular_distance(pointlist[i], pointlist[0], pointlist[end]);
		if (d > dmax) {
			index = i;
			dmax = d;
		}
	}

	// If max distance is greater than epsilon, recursively simplify
	if(dmax > epsilon) {
		PoolVector2Array firstline, lastline;
		const PoolVector2Array::Read pldata = pointlist.read();
		for(int i=0; i<index+1; i++)
			firstline.append(pldata[i]);
		const size_t plsize = pointlist.size();
		for(int i=index; i<plsize; i++)
			lastline.append(pldata[i]);

		PoolVector2Array r1 = this->rdp_simplify(firstline, epsilon);
		PoolVector2Array r2 = this->rdp_simplify(lastline, epsilon);

		out = r1;
		out.append_array(r2);
		if(out.size() < 3)
			return pointlist;
	} else {
		out.push_back(pointlist[0]);
		out.push_back(pointlist[end]);
	}

	return out;
}
real_t PolyVector::perpendicular_distance(const Vector2 &pt, const Vector2 &start, const Vector2 &end)
{
	Vector2 d(end.x-start.x, end.y-start.y);
	real_t mag = pow(pow(d.x,2.0)+pow(d.y,2.0),0.5);
	if(mag > 0.0)
		d /= mag;
	Vector2 pv(pt.x-start.x, pt.y-start.y);
	real_t pvdot = d.dot(pv);
	Vector2 a = pv-Vector2(pvdot*d.x, pvdot*d.y);
	return pow(pow(a.x,2.0)+pow(a.y,2.0),0.5);
}
