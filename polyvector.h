#ifndef POLYVECTOR_H
#define POLYVECTOR_H

#include <vector>
#include <map>

#include <core/math/camera_matrix.h>
#include <core/os/os.h>
#include <scene/main/viewport.h>
#include <scene/3d/camera.h>
#include <scene/3d/visual_instance.h>
#include <scene/3d/mesh_instance.h>
#include <scene/animation/animation_player.h>
#include <scene/resources/curve.h>

#include "polyvector_server.h"

#include "resource_importer_swf.h"
#include "earcut.hpp/earcut.hpp"

#define POLYVECTOR_MAX_QUALITY	10

using N = uint32_t;
using Coord = float;
using Point = Vector2;
namespace mapbox {
namespace util {
template <> struct nth<0, Vector2> { inline static auto get(const Vector2 &v) { return v.x; }; };
template <> struct nth<1, Vector2> { inline static auto get(const Vector2 &v) { return v.y; }; };
}
}



class PolyVector : public VisualInstance {
	GDCLASS(PolyVector, VisualInstance)

public:
	PolyVector();
	~PolyVector();

	void set_vector_image(const Ref<JSONVector>&);
	_FORCE_INLINE_ Ref<JSONVector> get_vector_image() const { return this->dataVectorFile; }
	void set_time(real_t);
	_FORCE_INLINE_ real_t get_time() { return this->rTime; }
	void set_lod_max_quality(int8_t);
	_FORCE_INLINE_ int8_t get_lod_max_quality() { return this->iMaxLod; }
	void set_lod_distance_scale(real_t);
	_FORCE_INLINE_ real_t get_lod_distance_scale() { return this->rLodDistanceScale; }
	void set_lod_distance_curve(real_t);
	_FORCE_INLINE_ real_t get_lod_distance_curve() { return this->rLodDistanceCurve; }
	void set_lod_reduction_factor(real_t);
	_FORCE_INLINE_ real_t get_lod_reduction_factor() { return this->rLodEpsilon; }
	void set_layer_separation(real_t);
	_FORCE_INLINE_ real_t get_layer_separation() { return this->rLayerDepth; }
	void set_material(const Ref<Material>&);
	_FORCE_INLINE_ Ref<Material> get_material() { return this->materialDefault; }
	void set_cast_shadow(GeometryInstance::ShadowCastingSetting);
	_FORCE_INLINE_ GeometryInstance::ShadowCastingSetting get_cast_shadow() const { return this->scsCastShadows; }
	void set_max_tessellation_angle(real_t);
	_FORCE_INLINE_ real_t get_max_tessellation_angle() { return this->rMaxTessellationAngle; }

	virtual AABB get_aabb() const { if(this->dataVectorFile.is_valid()) return PolyVectorServer::get_singleton()->get_aabb(this->dataVectorFile->get_hash(), this->iCurrentFrame, this->iCurrentLod); return AABB(); }
	virtual PoolVector<Face3> get_faces(uint32_t p_usage_flags) const;

	int8_t get_current_lod() { return this->iCurrentLod; }
	uint32_t get_vertex_count() { return this->iVertexCount; }

protected:
	static void _bind_methods();
	void _notification(int);

private:
	void _get_lod_from_camera_distance();
	void _draw_current_frame(bool forcedirty=false);
	void _clear_mesh_instances();

	static void _thread_mesh_builder_start(void*);
	bool _thread_mesh_builder();
	void _thread_mesh_builder_finish(uint16_t, int8_t);

	PoolVector2Array vw_simplify(const PoolVector2Array&,uint8_t);
	_FORCE_INLINE_ real_t area_of_triangle(Vector2,Vector2,Vector2);
	PoolVector2Array rdp_simplify(const PoolVector2Array&,double);
	_FORCE_INLINE_ real_t perpendicular_distance(const Vector2&,const Vector2&,const Vector2&);

	#ifndef NO_THREADS
	Thread *threadMeshBuilder = NULL;
	mutable Mutex *mutexMeshBuilder = NULL;
	#endif
	mutable Ref<JSONVector> dataVectorFile;
	Ref<SpatialMaterial> materialDefault;
	GeometryInstance::ShadowCastingSetting scsCastShadows = GeometryInstance::SHADOW_CASTING_SETTING_ON;
	MeshInstanceMap mapMeshDisplay;
	mutable AABB aabbBounds;
	mutable PoolVector<Face3> pvFaces;
	uint16_t iCurrentFrame = UINT16_MAX;
	int8_t iCurrentLod = INT8_MIN;
	uint32_t iVertexCount = 0;

	bool bFrameDirty = false;
	real_t rTime = 0.0;
	int8_t iMaxLod = 0;
	real_t rLodDistanceScale = 1.0;
	real_t rLodDistanceCurve = 0.55;
	real_t rLodEpsilon = 0.025;
	real_t rLayerDepth = 0.001;
	real_t rMaxTessellationAngle = 4.0;

	List<PolyVectorFrame> lFrameData;
	List<PolyVectorCharacter> lDictionaryData;
	real_t rFps;
};

#endif	// POLYVECTOR_H
